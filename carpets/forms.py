from django import forms

from carpets.models import Palette


class PaletteForm(forms.ModelForm):
    class Meta:
        model = Palette
        exclude = ('user', 'image')

    def clean_data(self):
        data = self.cleaned_data['data']
        data['from_colors'] = filter(bool, data.get('from_colors'))
        data['to_colors'] = filter(bool, data.get('to_colors'))
        return data