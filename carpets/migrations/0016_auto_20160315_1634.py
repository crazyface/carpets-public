# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0015_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='color',
            name='order',
            field=models.IntegerField(unique=True),
        ),
    ]
