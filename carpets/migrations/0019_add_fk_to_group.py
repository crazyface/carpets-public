# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def add_fk_to_group(apps, schema_editor):
    Color = apps.get_model("carpets", "Color")
    ColorGroup = apps.get_model("carpets", "ColorGroup")
    for color in Color.objects.all():
        group = ColorGroup.objects.get_or_create(name=color.group)[0]
        color.group_obj = group
        color.save()


class Migration(migrations.Migration):
    dependencies = [
        ('carpets', '0018_color_group_obj'),
    ]

    operations = [
        migrations.RunPython(add_fk_to_group),
    ]
