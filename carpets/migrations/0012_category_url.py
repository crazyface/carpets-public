# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_id_to_url_field(apps, schema_editor):
    Category = apps.get_model("carpets", "Category")
    for category in Category.objects.all():
        category.url = category.id
        category.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0011_category_url'),
    ]

    operations = [
        migrations.RunPython(add_id_to_url_field),
    ]
