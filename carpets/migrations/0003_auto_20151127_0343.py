# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0002_color'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='room',
            name='rotation',
        ),
        migrations.RemoveField(
            model_name='room',
            name='vertical_scale',
        ),
        migrations.AddField(
            model_name='room',
            name='left_offset',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='room',
            name='repeat',
            field=models.FloatField(default=4),
        ),
        migrations.AddField(
            model_name='room',
            name='rotate_x',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='room',
            name='rotate_y',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='room',
            name='rotate_z',
            field=models.FloatField(default=0),
        ),
        migrations.AddField(
            model_name='room',
            name='top_offset',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='category',
            name='parent',
            field=models.ForeignKey(blank=True, editable=False, to='carpets.Category', null=True),
        ),
    ]
