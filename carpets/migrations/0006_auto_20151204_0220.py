# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0005_merge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='room',
            name='rotate_x',
            field=models.FloatField(default=70, verbose_name=b'Perspective'),
        ),
        migrations.AlterField(
            model_name='room',
            name='rotate_y',
            field=models.FloatField(default=50, verbose_name=b'Horizon'),
        ),
    ]
