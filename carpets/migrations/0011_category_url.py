# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0010_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='url',
            field=models.CharField(default=datetime.datetime(2016, 3, 15, 18, 10, 31, 148329, tzinfo=utc), max_length=255),
            preserve_default=False,
        ),
    ]
