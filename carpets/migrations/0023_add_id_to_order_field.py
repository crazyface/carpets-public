# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def add_id_to_color_group(apps, schema_editor):
    ColorGroup = apps.get_model("carpets", "ColorGroup")
    for i, color in enumerate(ColorGroup.objects.all()):
        color.order = i
        color.save()


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0022_auto_20160331_1211'),
    ]

    operations = [
        migrations.RunPython(add_id_to_color_group),
    ]
