# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('carpets', '0008_auto_20160301_0452'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='meta',
            field=models.TextField(help_text=b'Example: <br>&lt;meta name="keywords" content="..." /&gt; <br>&lt;meta name="description" content="..." /&gt;', null=True, blank=True),
        ),
    ]
