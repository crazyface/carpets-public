# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Carpet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('item_code', models.CharField(unique=True, max_length=30)),
                ('image', models.ImageField(upload_to=b'carpet/images')),
                ('active', models.BooleanField(default=True)),
                ('description', models.TextField(max_length=300, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('active', models.BooleanField(default=True)),
                ('description', models.TextField(max_length=300, blank=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('cover', models.ImageField(upload_to=b'category_cover')),
                ('parent', models.ForeignKey(blank=True, to='carpets.Category', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Palette',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', jsonfield.fields.JSONField(default={})),
                ('image', models.ImageField(upload_to=b'palette/images')),
                ('carpet', models.ForeignKey(to='carpets.Carpet')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('room_code', models.CharField(max_length=50)),
                ('image', models.ImageField(upload_to=b'rooms/images')),
                ('active', models.BooleanField(default=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('scale', models.FloatField(default=0.5)),
                ('vertical_scale', models.FloatField(default=0.3)),
                ('rotation', models.FloatField(default=45)),
            ],
        ),
        migrations.AddField(
            model_name='carpet',
            name='category',
            field=models.ManyToManyField(related_name='category_carpets', to='carpets.Category'),
        ),
    ]
