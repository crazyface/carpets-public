import uuid
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http.response import JsonResponse, Http404
from django.shortcuts import render
from django.views.decorators.http import require_POST
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView
from django.core.files.uploadedfile import InMemoryUploadedFile
from .models import Carpet, Room, Category, Palette
from django.http import HttpResponse
from PIL import Image
import re
import time
from django.shortcuts import get_object_or_404
import PIL
from carpets.forms import PaletteForm
from carpets.utils.decorators import ajax_required
from django.db.models import Q
import StringIO
from slugify import slugify
from django.core.files.storage import default_storage


def recolor_carpet(request, item_code):
    previous_category = None
    category_id = request.GET.get('category')
    carpet = get_object_or_404(Carpet, item_code=item_code,
                               active=True)
    categories = carpet.category.all()
    if categories.count() > 1 and category_id:
        previous_category = carpet.category.filter(pk=category_id).first()
    category = previous_category or carpet.category.filter(active=True).first()
    rooms = Room.objects.filter(active=True, categories=category)
    context = {
        'carpet': carpet,
        'rooms': rooms,
        'category': category,
    }
    return render(request, 'carpets/recolor_carpet.html', context)


def replace_colors(request, item_code):
    carpet = get_object_or_404(Carpet, item_code=item_code)
    from_colors = filter(bool, request.GET.get('from', '').split(','))
    to_colors = filter(bool, request.GET.get('to', '').split(','))
    image = carpet.replace_colors(from_colors, to_colors)
    image.thumbnail([600, 600], PIL.Image.ANTIALIAS)
    response = HttpResponse(content_type='image/jpeg')
    response['Content-Disposition'] = 'filename="{}.png"'.format(carpet.name)
    image.save(response, 'JPEG', quality=90)
    return response


def download_pcx(request, pk):
    palette = get_object_or_404(Palette, pk=pk)
    carpet = palette.carpet
    from_colors = palette.data.get('from_colors', [])
    to_colors = palette.data.get('to_colors', [])
    image = carpet.replace_colors(from_colors, to_colors)
    image = image.convert('RGB')
    if request.user.is_superuser:
        img_format = 'PCX'
    else:
        raise Http404
    response = HttpResponse(content_type='octet/stream')
    response['Content-Disposition'] = 'filename="{}.{}"'.format(carpet.name, img_format.lower())
    image.save(response, img_format)
    return response


def fill(output, pattern):
    width, height = output.size
    x = 0
    y = 0
    while x < width:
        y = 0
        while y < height:
            output.paste(pattern, (x, y))
            y += pattern.size[1]
        x += pattern.size[0]
    return output


# def room_preview(request, item_code, room_id):
#     carpet = get_object_or_404(Carpet, item_code=item_code)
#     room = get_object_or_404(Room, id=room_id)
#     from_colors = filter(bool, request.GET.get('from', '').split(','))
#     to_colors = filter(bool, request.GET.get('to', '').split(','))
#     image = carpet.replace_colors(from_colors, to_colors)
#     scale = room.scale
#     vertical_scale = room.vertical_scale
#     rotation = room.rotation
#     image.thumbnail([image.size[0] * scale,
#                      image.size[1] * scale],
#                     Image.ANTIALIAS)
#     room_image = Image.open(room.image)
#
#     output = Image.new("RGB", (image.size[0] * 2, image.size[1] * 2))
#     output = fill(output, image)
#     output = output.rotate(-rotation, PIL.Image.BICUBIC, expand=True)
#     output = output.resize([image.size[0] * 2,
#                             int(image.size[1] * 2 * vertical_scale)],
#                            PIL.Image.ANTIALIAS)
#
#     output = output.crop([int(output.size[0] * 0.25),
#                           int(output.size[1] * 0.25),
#                           int(output.size[0] * 0.75),
#                           int(output.size[1] * 0.75), ])
#
#     res = Image.new("RGBA", room_image.size)
#     res = fill(res, output)
#     res.paste(room_image, (0, 0), room_image)
#     response = HttpResponse(content_type='image/png')
#     response['Content-Disposition'] = 'filename="{}.png"'.format(carpet.name)
#     res.save(response, 'PNG')
#     return response


# def recolor_image(request):
#     if request.method == "POST":
#         carpet = request.POST.get("carpet")
#         room = request.POST.get("room")
#         datauri = carpet
#         imgstr = re.search(r'base64,(.*)', datauri).group(1)
#         new_carpet = "/app/media/carpet/images/carpet_img_" + str(
#             time.time()).replace(".", "") + ".png"
#         output = open(new_carpet, 'wb')
#         output.write(imgstr.decode('base64'))
#         output.close()
#         foreground = Image.open("/app/" + room)
#         width, height = foreground.size
#         sizes = width, height
#         im = Image.open(new_carpet)
#         im_resized = im.resize(sizes, Image.ANTIALIAS)
#         im_resized.save(new_carpet, "PNG")
#         # im = Image.open(new_carpet)
#         # im.save(new_carpet, dpi=(width,height))
#         background = Image.open(new_carpet)
#         background.paste(foreground, (0, 0), foreground)
#         blended_img_name = "room_carpet_" + str(time.time()).replace(".",
#                                                                      "") + ".JPEG"
#         blended_img = "/app/media/carpet/images/" + blended_img_name
#         background.save(blended_img)
#         return HttpResponse(blended_img_name)


@ajax_required
@require_POST
def palette_create_ajax_view(request):
    form = PaletteForm(data=request.POST)
    if form.is_valid():
        palette = form.save(commit=False)
        carpet = palette.carpet
        from_colors = filter(bool, palette.data.get('from_colors', ''))
        to_colors = filter(bool, palette.data.get('to_colors', ''))
        image = carpet.replace_colors(from_colors, to_colors)
        image.thumbnail([600, 600], PIL.Image.ANTIALIAS)
        image_io = StringIO.StringIO()
        image.save(image_io, format='PNG')
        name = '{}-{}.png'.format(slugify(carpet.name), uuid.uuid4())
        image_file = InMemoryUploadedFile(image_io, None, name, 'image/png',
                                          image_io.len, None)
        palette.image = image_file
        if request.user.is_authenticated():
            palette.user = request.user
        form.save()
        return JsonResponse({"id": palette.id}, status=201)
    return JsonResponse(form.errors, status=400)


class CategoryListView(ListView):
    model = Carpet
    template_name = 'carpets/category_list.html'
    paginate_by = 9

    def get_queryset(self):
        url = self.kwargs.get('url')
        qs = super(CategoryListView, self).get_queryset().filter(active=True)
        self.category = None
        if url:
            self.category = get_object_or_404(Category, url=url)
            category_id = self.category.id
            if self.category.parent:
                qs = qs.filter(category__id=category_id)
            else:
                qs = qs.filter(Q(category__parent__id=category_id) | Q(category__id=category_id)).distinct()
        return qs

    def get_context_data(self, **kwargs):
        context = super(CategoryListView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.filter(parent__isnull=True
                                ).prefetch_related('category_set').filter(active=True)
        context['category'] = self.category
        return context


class PaletteListView(ListView):
    model = Palette
    template_name = 'carpets/palette_list.html'
    paginate_by = 6

    def get_queryset(self):
        self.queryset = self.request.user.palette_set.all()
        return super(PaletteListView, self).get_queryset()


class PaletteDeleteView(DeleteView):
    model = Palette
    success_url = reverse_lazy('palettes')

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.user == self.request.user:
            context = self.get_context_data(object=self.object)
            return self.render_to_response(context)
        raise Http404


def room_preview(request, item_code, room_id):
    carpet = get_object_or_404(Carpet, item_code=item_code)
    room = get_object_or_404(Room, id=room_id)
    from_colors = filter(bool, request.GET.get('from', '').split(','))
    to_colors = filter(bool, request.GET.get('to', '').split(','))
    image = carpet.replace_colors(from_colors, to_colors)
    preview = room.get_carpet_preview(image)
    response = HttpResponse(content_type='image/jpeg')
    response['Content-Disposition'] = 'filename="{}.png"'.format(carpet.name)
    preview.save(response, 'JPEG', quality=90)
    return response


def tune_room(request, room_id):
    room = get_object_or_404(Room, id=room_id)
    import os
    path = os.path.join(os.path.dirname(__file__), 'demo.png')
    image = Image.open(path)
    preview = room.get_carpet_preview(image,
                  scale=float(request.GET.get('scale', room.scale)),
                  repeat=float(request.GET.get('repeat', room.repeat)),
                  left_offset=float(request.GET.get('left_offset', room.left_offset)),
                  top_offset=float(request.GET.get('top_offset', room.top_offset)),
                  rotate_x=float(request.GET.get('rotate_x', room.rotate_x)),
                  rotate_y=float(request.GET.get('rotate_y', room.rotate_y)),
                  rotate_z=float(request.GET.get('rotate_z', room.rotate_z)),
                               )
    response = HttpResponse(content_type='image/jpeg')
    # response['Content-Disposition'] = 'filename="{}.png"'.format(carpet.name)
    preview.save(response, 'JPEG', quality=90)
    return response


@ajax_required
@login_required
def logout_ajax(request):
    if request.method == 'POST':
        logout(request)
        return JsonResponse({'success': True})
    return render(request, 'carpets/logout.html', {})


@ajax_required
def search(request):
    carpets = []
    q = request.GET.get('q')
    if q:
        carpets = Carpet.objects.filter(name__icontains=q).exclude(
            active=False).exclude(category__isnull=True)
    res = [{'url': str(c.get_url()), 'name': c.name} for c in carpets]
    return JsonResponse({'result': res})