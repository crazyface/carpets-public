from PIL import Image, ImageDraw
import math
import numpy


class Point3D:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x, self.y, self.z = float(x), float(y), float(z)

    def rotateX(self, angle):
        """ Rotates the point around the X axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        y = self.y * cosa - self.z * sina
        z = self.y * sina + self.z * cosa
        return Point3D(self.x, y, z)

    def rotateY(self, angle):
        """ Rotates the point around the Y axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        z = self.z * cosa - self.x * sina
        x = self.z * sina + self.x * cosa
        return Point3D(x, self.y, z)

    def rotateZ(self, angle):
        """ Rotates the point around the Z axis by the given angle in degrees. """
        rad = angle * math.pi / 180
        cosa = math.cos(rad)
        sina = math.sin(rad)
        x = self.x * cosa - self.y * sina
        y = self.x * sina + self.y * cosa
        return Point3D(x, y, self.z)

    def project(self, win_width, win_height, fov, viewer_distance):
        """ Transforms this 3D point to 2D using a perspective projection. """
        factor = fov / (viewer_distance + self.z)
        x = self.x * factor + win_width / 2
        y = -self.y * factor + win_height / 2
        return Point3D(x, y, 1)


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0]*p1[0], -p2[0]*p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1]*p1[0], -p2[1]*p1[1]])

    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)


def fill(output, pattern):
    width, height = output.size
    x = 0
    y = 0
    while x < width:
        y = 0
        while y < height:
            output.paste(pattern, (x, y))
            y += pattern.size[1]
        x += pattern.size[0]
    return output


def get_rotated(input, angle_x=0, angle_y=0, angle_z=0, repeat=4):
    width, height = input.size
    img = Image.new("RGB", (width,
                            height), "white")
    input.thumbnail((width/repeat, height/repeat))
    img = fill(img, input)
    w, h = img.size
    a = Point3D(-(w/2), -(h/2), 0)
    b = Point3D(-(w/2), h/2, 0)
    c = Point3D(w/2, h/2, 0)
    d = Point3D(w/2, -(h/2), 0)


    # a = a.rotateX(angle_x).rotateY(angle_y).rotateZ(angle_z).project(w, h, w, w)
    # b = b.rotateX(angle_x).rotateY(angle_y).rotateZ(angle_z).project(w, h, w, w)
    # c = c.rotateX(angle_x).rotateY(angle_y).rotateZ(angle_z).project(w, h, w, w)
    # d = d.rotateX(angle_x).rotateY(angle_y).rotateZ(angle_z).project(w, h, w, w)


    a = a.rotateZ(angle_z).rotateY(angle_y).rotateX(angle_x).project(w, h, w, w)
    b = b.rotateZ(angle_z).rotateY(angle_y).rotateX(angle_x).project(w, h, w, w)
    c = c.rotateZ(angle_z).rotateY(angle_y).rotateX(angle_x).project(w, h, w, w)
    d = d.rotateZ(angle_z).rotateY(angle_y).rotateX(angle_x).project(w, h, w, w)
    coeffs = find_coeffs(
        [(a.x, a.y), (b.x, b.y), (c.x, c.y), (d.x, d.y)],
        [(0, 0), (w, 0), (w, h), (0, h)])
    img = img.transform((w, h), Image.PERSPECTIVE, coeffs,
            Image.BICUBIC)

    return img