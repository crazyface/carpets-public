$(document).ready(function () {
    var from_colors = [];
    var to_colors = [];
    var room = null;
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
    var hash = getUrlParameter('hash');
    if(hash){
        document.location.hash = hash;
        document.location.search = '';
    }
    hash = $.cookie('hash');
    $.removeCookie('hash');
    if(!hash){
        hash = decodeURIComponent(document.location.hash);
    }

    hash = hash.replace('#', '');

    function setColor(from_color, to_color) {
        var to_color = $(".colorbox.hand[value='" + to_color + "']");
        var from_color = $(".change_color_item[value='" + from_color + "']");
        from_color.find('.colorbox_item').css('background-color', '#' + to_color.attr('value'));
        from_color.find('.color_name').text(to_color.data('name'));
    };

    if (hash.length) {
        var data = JSON.parse(hash);
        from_colors = data.from_colors;
        to_colors = data.to_colors;
        room = data.room;
        $(from_colors).each(function (index, val) {
            setColor(val, to_colors[index]);
            //var to_color = $(".colorbox.hand[value='" + to_colors[index] + "']");
            //var from_color = $(".change_color_item[value='" + val + "']");
            //from_color.find('.colorbox_item').css('background-color', '#' + to_color.attr('value'));
            //from_color.find('.color_name').text(to_color.data('name'));
        });
    }
    refresh_url();

    function sendData(url, data) {
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            statusCode: {
                201: function (data) {
                    var $alert = $('<div class="alert alert-success">' +
                                  '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                  '<strong>Saved!</strong> Reference ID: "' + data.id + '"</div>');
                    $("#message").append($alert);
                },
                400: function (response) {
                    if($('.no_change').length == 0){
                        var $alert = $('<div class="alert alert-warning no_change">' +
                                      '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' +
                                      '<strong>Nothing was changed!</strong></div>');
                        $("#message").append($alert);
                    }
                }
            }
        });
    }

    var from_color;
    $('#preview').on('load ready', function () {
        $('.loader').addClass('hidden');
        $('#preview').removeClass('hidden');
    });
    var cnt = 0;
    function refresh_url() {
        $('#preview').removeAttr('src');
        $('.loader').removeClass('hidden');
        $('#preview').addClass('hidden');
        var src = $('#preview').attr('default-src');
        var query = '';
        if (room) {
            src += room + '/';
        }
        src += '?';
        query += 'from=' + from_colors.join(',');
        query += '&to=' + to_colors.join(',');

        query += '&cnt=' + cnt;
        cnt += 1;
        $('#preview').attr('src', src + query);
        if (from_colors.length || room) {
            document.location.hash = JSON.stringify({
                from_colors: from_colors,
                to_colors: to_colors,
                room: room
            });
        } else {
            document.location.hash = '';
        }
    }
    $('.change_color_item').click(function (event) {
        from_color = $(this);
        $('#modal_colorpicker').modal('show');
    });
    $(document).on('click', '#reset_but', function (event) {
        from_colors = [];
        to_colors = [];
        $('.change_color_item').each(function () {
            var $this = $(this);
            $this.find('.colorbox_item').css('background-color', '#' + $this.data('value'));
            $this.find('.color_name').text($this.data('name'));
        });
        refresh_url();
    });
    
    $('#modal_colorpicker').on('click', '.colorbox.hand', function (event) {
        to_color = $(this);
        $('#modal_colorpicker').modal('hide');
        if (from_color.length && to_color.length) {
            var f_color = from_color.attr('value');
            var t_color = to_color.attr('value');
            var index = from_colors.indexOf(f_color);
            if (index == -1) {
                from_colors.push(f_color);
                to_colors.push(t_color);
            } else {
                to_colors[index] = t_color;
            }
            //from_color.css('background-color', '#' + t_color);
            setColor(f_color, t_color);
            refresh_url();
        }
    });


    $("body")
        .on('click', '#save', function (e) {
          var asked = $.cookie('asked');
          var user = $('body').data('user');
         e.preventDefault();
          if(asked || user != 'None'){
            var url = $(this).data('url');
            data = {
                "carpet": $(this).data('carpet-id'),
                "data": window.location.hash.substr(1)
            };
            sendData(url, data);
          }else{
            $.cookie('asked', true);
            //$.cookie('hash', document.location.hash);
            $('#loginBtn').click();
            //$('.auth_url').each(function(){
            //   var $this = $(this);
            //    $this.attr('href', $this.data('href') + '?next=' + encodeURI(document.location.pathname))
            //});
          }
        });

    $(document).on('click', '#choose_room', function (event) {
        from_color = $(this);
        $('#choose_room_modal').modal('show');
    });

    $(document).on('click', '.roomchoice_item', function (event) {
        room = $(this).attr('value');
        refresh_url();
        $('#choose_room_modal').modal('hide');
    });
    $(document).on('click', '#edit_palette', function (event) {
        room = null;
        refresh_url();
    });
});
