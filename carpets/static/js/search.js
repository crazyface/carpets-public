$( document ).ready(function() {
    // Instantiate the Bloodhound suggestion engine
    var carpets = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            wildcard: '%QUERY',
            url: document.location.origin + '/search/?q=%QUERY',
            ajax: {
                jsonp: 'callback',
                dataType: 'jsonp'
            },
            transform: function (response) {
                // Map the remote source JSON array to a JavaScript object array
                return $.map(response.result, function (carpet) {
                    return {
                        url: carpet.url,
                        value: carpet.name
                    };
                });
            }
        }
    });
    window.initTypeahead = function () {
        $('.typeahead')
            .typeahead(null, {
                display: 'value',
                source: carpets,
                templates: {
                    empty: [
                        '<p class="empty-message alert-info">',
                        'Unable to find any carpet that match current query',
                        '</p>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<a href="{{url}}" class="drop-down-item">{{value}}</a>')
                }
            })
            .on("keypress", function (event) {
                if (event.which == 13) {
                    var name = 'q=' + $(this).val();
                    getCarpet(name);
                }
            });
    };
    function getCarpet(data) {
        $.ajax({
            type: "GET",
            url: document.location.origin + '/search/',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.result.length !== 0) {
                    var url = data.result[0].url;
                    window.location.replace(url)
                }
            }
        });
    }
});
