$(document).ready(function () {
    $(document).on('click', '#print-btn', function () {
        var $this = $(this);
        var imageUrl = $('#preview').attr('src');
        var category = $this.data('carpet-category');
        var json = location.href.split("#")[1];
        if(json){
            var obj = $.parseJSON(json);
        }else{
            obj = {
                from_colors: [],
                to_colors: []
            }
        }
        var from = obj.from_colors;
        var to = obj.to_colors;
        var carpetName = $this.data('carpet-name');
        //var image = $('<image src="' + imageUrl + '">');
        var image = $('<img>');
        var imageContainer = $('<div class="" id="img-container" style=""></div>');
        var colors = '';
        $.each(from.slice(1), function (i, item) {
            colors += '<tr>'
                + '<td></td><td></td>'
                + '<td>' + item + ' ' +'<span style="color: ' + '#' + item + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>'
                + '<td>' + to[i + 1] + ' ' + '<span style="color: ' + '#' + to[i + 1] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' +'</td>'
                + '</tr>';
        });
        var table = '<table class="table table-bordered" border="1">' + '<tbody>' + '<tr>' +
            '<th>Category</th>' +
            '<th>Name</th>' +
            '<th>From color</th>' +
            '<th>To color</th>' +
            '</tr>' +
            '<tr>' +
            '<td>' + category + '</td>' +
            '<td>' + carpetName + '</td>' +
            '<td>' + from[0] + ' ' + '<span style="color: ' + '#' + from[0] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '<td>' + to[0] + ' ' + '<span style="color: ' + '#' + to[0] + '!important;' + '  ">&#9608;&#9608;&#9608;&#9608;&#9608;</span>' + '</td>' +
            '</tr>' + colors +
            '</tbody>' + '</table>';
        imageContainer.append(table);
        imageContainer.append(image);
        image.load(function () {
            imageContainer.print({
                iframe: true,
                timeout: 2000,
            });
            //imageContainer.remove();
        });
        //$('body').append(imageContainer);
        image.attr('src', imageUrl + '&stamp=' + (new Date()).getTime() );
    });
});