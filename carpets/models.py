import tempfile
from django.core.exceptions import ValidationError
from django.core.files.base import File
from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings

from PIL import Image
from wand.image import Image as WandImage
from sorl.thumbnail import get_thumbnail
import numpy as np
from jsonfield import JSONField
import json
from utils.rotate import get_rotated


def validate_image(instance):
    try:
        Image.open(instance).getcolors()
    except IOError:
        raise ValidationError(
            message='Unsupported or broken file.',
            code='image_error',
            params=()
        )


class Category(models.Model):
    name = models.CharField(max_length=100, blank=False)
    active = models.BooleanField(default=True)
    description = models.TextField(max_length=300, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    parent = models.ForeignKey('self', null=True, blank=True,
                               limit_choices_to={'parent__isnull': True})
    cover = models.ImageField(upload_to='category_cover')
    url = models.CharField(max_length=255, unique=True)
    meta = models.TextField(null=True, blank=True,
                            help_text='Example: <br>'
                                      '&lt;meta name="keywords" content="..." /&gt; <br>'
                                      '&lt;meta name="description" content="..." /&gt;')

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __unicode__(self):
        return self.name

    def is_parent(self):
        return self.category_set.exists()


class Carpet(models.Model):
    category = models.ManyToManyField(Category,
                                      related_name="category_carpets", )
    name = models.CharField(blank=False, max_length=100)
    item_code = models.CharField(max_length=30, unique=True)
    image = models.ImageField(upload_to='carpet/images')
    active = models.BooleanField(default=True)
    description = models.TextField(max_length=300, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def get_thumbnail(self, geometry='600'):
        return get_thumbnail(self.image, geometry, format='PNG', quality=100)

    def preview(self):
        try:
            url = self.get_thumbnail('100').url
        except Exception:
            url = 'https://placeholdit.imgix.net/~text?txtsize=20&txt=No+image+or+corrupted+image&w=100&h=100&txttrack=0'
        return u"<img src='{}'>".format(url)


    preview.allow_tags = True

    def save(self, *args, **kwargs):
        image = self.image
        if image:
            original = WandImage(file=image.file)
            converted = original.convert('pcx')
            tmp_file = tempfile.NamedTemporaryFile()
            converted.save(file=tmp_file)
            self.image = File(tmp_file, image.name)
        super(Carpet, self).save(*args, **kwargs)

    def color_list(self):
        return Image.open(self.image).convert('RGB').getcolors() or []

    def hex_color_list(self):
        return ['%02x%02x%02x' % color[1] for color in self.color_list()]

    def get_colors(self):
        res = []
        color_list = self.hex_color_list()
        color_names = dict(Color.objects.filter(hex__in=color_list).values_list('hex', 'name'))
        for i, color in enumerate(color_list):
            res.append({'name': color_names.get(color, i+1),
                        'hex': color})
        return res

    def __unicode__(self):
        return str(self.name)

    def replace_color(self, im, input_color, output_color):
        if input_color == output_color:
            return im
        data = np.array(im)  # "data" is a height x width x 4 numpy array
        red, green, blue, alpha = data.T  # Temporarily unpack the bands for readability
        # Replace white with red... (leaves alpha values alone...)
        white_areas = (red == input_color[0]) & (green == input_color[1]) & (
            blue == input_color[2])
        data[..., :-1][white_areas.T] = output_color  # Transpose back needed
        return Image.fromarray(data)

    def replace_colors(self, from_colors=None, to_colors=None):
        from_colors = from_colors or []
        to_colors = to_colors or []
        im = Image.open(self.image).convert('RGBA')
        for from_color, to_color in zip(from_colors, to_colors):
            from_color = self.hex_to_rgb(from_color)
            to_color = self.hex_to_rgb(to_color)
            im = self.replace_color(im, from_color, to_color)
        return im

    def get_replace_url(self):
        return reverse('replace_colors', args=[self.item_code])

    def hex_to_rgb(self, colorstring):
        """ convert a hex color string  to an RGB array """
        colorstring = colorstring.strip()
        r, g, b = colorstring[:2], colorstring[2:4], colorstring[4:]
        return [int(val, 16) for val in [r, g, b]]

    def get_url(self):
        return reverse('recolor_carpet', args=[self.item_code])


class Room(models.Model):
    name = models.CharField(blank=False, max_length=50)
    room_code = models.CharField(max_length=50, blank=False)
    image = models.ImageField(upload_to='rooms/images')
    active = models.BooleanField(default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    scale = models.FloatField(default=1.6)
    repeat = models.FloatField(default=4.0)
    rotate_x = models.FloatField('Perspective', default=70)
    rotate_y = models.FloatField('Horizon', default=50)
    rotate_z = models.FloatField('Orientation', default=0)
    top_offset = models.FloatField(default=0)
    left_offset = models.FloatField(default=0)
    categories = models.ManyToManyField('Category')

    def get_carpet_preview(self, image, scale=None,
                           repeat=None, left_offset=None, top_offset=None,
                           rotate_x=None, rotate_y=None, rotate_z=None):
        room_image = Image.open(self.image)
        room_image.thumbnail([500, 500],
                        Image.ANTIALIAS)
        scale = scale if scale is not None else self.scale
        repeat = repeat if repeat is not None else self.repeat
        left = left_offset if left_offset is not None else self.left_offset
        top = top_offset if top_offset is not None else self.top_offset
        angle_x = rotate_x if rotate_x is not None else self.rotate_x
        angle_y = rotate_y if rotate_y is not None else self.rotate_y
        angle_z = rotate_z if rotate_z is not None else self.rotate_z
        size = room_image.size
        left = left / 100.0
        top = top / 100.0
        image = image.resize([int(size[0] * scale),
                        int(size[0] * scale)],
                        Image.ANTIALIAS)
        image = get_rotated(image, angle_x, angle_y, angle_z, repeat)
        res = Image.new("RGBA", room_image.size)
        offset_left = (res.size[0] - image.size[0]) / 2 + res.size[0] * left
        offset_top = (res.size[1] - image.size[1]) / 2 + res.size[1] * top
        res.paste(image, (int(offset_left), int(offset_top)))
        # res.show()
        res.paste(room_image, (0, 0), room_image)
        return res


    def __unicode__(self):
        return self.name


class Palette(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True)
    data = JSONField(default={})
    carpet = models.ForeignKey(Carpet)
    image = models.ImageField(upload_to='palette/images')

    def get_thumbnail(self, geometry='600'):
        return get_thumbnail(self.image, geometry, format='PNG', quality=100)

    def preview(self):
        try:
            return u"<img src='{}'>".format(self.get_thumbnail('100').url)
        except:
            return "error"
    preview.allow_tags = True

    def carpet_code(self):
        return self.carpet.item_code

    def download(self):
        url = reverse('download_pcx', args=[self.id])
        return u'<a href="{}">download</a>'.format(url)
    download.allow_tags = True

    def get_url(self):
        url = reverse('recolor_carpet', args=[self.carpet.item_code])
        return url + "?hash={}".format(json.dumps(self.data))

    def user_email(self):
        if self.user:
            return self.user.email


class ColorGroup(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255, null=True, blank=True)
    order = models.IntegerField(unique=True)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return u'{}'.format(self.name)


class Color(models.Model):
    group = models.ForeignKey(ColorGroup)
    name = models.CharField(max_length=255)
    hex = models.CharField(max_length=255)

    def __unicode__(self):
        return u'{} {}'.format(self.group, self.name)

    def color_preview(self):
        return '<span style="color: #{0}">&#9608;&#9608;&#9608;&#9608;&#9608;</span> {0}'.format(self.hex)
    color_preview.allow_tags = True
