from django.core.urlresolvers import reverse
from carpets.models import Category, Carpet, ColorGroup
from django import template

register = template.Library()


@register.assignment_tag
def get_categories():
    return Category.objects.filter(active=True).prefetch_related('category_set')


@register.assignment_tag
def get_carpets(slice=10):
    return Carpet.objects.all().order_by('?')[:slice]


@register.assignment_tag
def get_colors():
    return ColorGroup.objects.all()


@register.filter
def category_url(value):
    category = Category.objects.filter(name__icontains=value, active=True).prefetch_related('category_set').first()
    if category:
        return reverse('category_details', args=[category.url])
    return '#'
