from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext, ugettext_lazy as _
from carpets.models import Carpet, Room, Category, Palette, Color, ColorGroup


class CategoryAdmin(admin.ModelAdmin):
    list_filter = ('name',)
    list_display = ('name',)


class CarpetAdmin(admin.ModelAdmin):
    list_filter = ('name', 'item_code',)
    list_display = ('name', 'item_code', 'preview', 'description')


class RoomAdmin(admin.ModelAdmin):
    list_filter = ('name', 'active',)
    list_display = ('name', 'active')
    change_form_template = 'carpets/admin/room_change_form.html'


class PaletteInline(admin.TabularInline):
    model = Palette
    readonly_fields = ('id', 'data', 'carpet', 'image')
    fields = ('id', 'data', 'carpet', 'image')
    can_delete = False
    extra = 0


class UserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'classes': ['collapse'], 'fields': (
            'is_active', 'is_staff', 'is_superuser',
            'groups', 'user_permissions')}),
        (_('Important dates'), {'classes': ['collapse'], 'fields': (
            'last_login', 'date_joined')})
    )
    inlines = (PaletteInline, )


class PaletteAdmin(admin.ModelAdmin):
    search_fields = ['id']
    list_display = ('id', 'download', 'carpet_code', 'user_email', 'preview')


class ColorAdmin(admin.ModelAdmin):
    list_display = ('name', 'color_preview')


admin.site.register(Category, CategoryAdmin)
admin.site.register(Carpet, CarpetAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Color, ColorAdmin)
admin.site.register(Palette, PaletteAdmin)
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(ColorGroup)
