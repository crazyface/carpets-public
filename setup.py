"""
The purpose of this setup.py is to upload this reusable app to the Python
package index so that your potential users can easily install it via
``easy_install`` or ``pip`.  However, you might never need to use this since
you can just host your app on github or bitbucket and use ``pip`` to install it
from there.

In order to test this package with an existing Django project, activate that
project's virtualenv and run::

    python setup.py develop

This will build the app in the same folder and just add a reference to your
virtualenv's easy-install.pth file.

When you are ready for a release you can register your desired package name and
upload yout work.  You need an account at pypi.python.org for this::

    python setup.py register
    python setup.py sdist upload

For more information please see this guide:
http://guide.python-distribute.org/quickstart.html

Replace all occurrences of ``your-app-name``, ``package_name``, ``Your Name``,
``your-name``.

"""
from setuptools import setup, find_packages

setup(
    name="carpets",
    version='0.1b',
    description='Carpets App',
    long_description='Carpets App',
    license='The MIT License',
    platforms=['OS Independent'],
    keywords='carpets-recolor, app',
    author='crazyface',
    author_email='johnybit13@gmail.com',
    url="https://github.com/crazyface/carpets",
    packages=['carpets'],
    include_package_data=True,
    install_requires=[
        'Django>=1.8.4',
        'numpy>=1.10.1',
        'jsonfield>=1.0.3',
        'Pillow>=3.0.0',
        'sorl-thumbnail>=12.3',
        'Wand>=0.4.2'
    ]
)
